[![Project](https://badgen.net/badge/project/lizard/orange?icon=gitlab)](https://gitlab.com/bi_zeps/ca_lizard/-/blob/main/README.md#lizard)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%2Fissues_statistics)](https://gitlab.com/bi_zeps/ca_lizard/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%2Frepository%2Fcommits%3Fbranch%3Dmain)](https://gitlab.com/bi_zeps/ca_lizard/-/commits/main)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17864060%3Flicense%3Dtrue)](https://gitlab.com/bi_zeps/ca_lizard/-/blob/main/LICENSE) 
applies to software of this project. The running software within the image/container ships with its own license(s).

[![Stable Version](https://img.shields.io/docker/v/bizeps/lizard/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/ca_lizard/-/blob/main/CHANGELOG.md#lizard)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/ca_lizard?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/lizard)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/ca_lizard/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/lizard)

# Lizard
Image with [Lizard](https://pypi.org/project/lizard/) installed. Lizard is a code analyzer without caring the C/C++ header files. It works with Java, C/C++, JavaScript, Python, Ruby, Swift, Objective C. Metrics includes cyclomatic complexity number etc.

See also [Lizard Homepage](http://www.lizard.ws/) or [Lizard on Github](https://github.com/terryyin/lizard).

`docker run --rm bizeps/lizard:latest lizard`, uses working directory `/var/build`.

Another example to call with 'modified cyclomatic complexity', set nlog threshold to 200 and cyclomatic complexity threshold to 15 and create 'report.txt' file:
`docker run -v $(pwd):/var/build --rm bizeps/lizard:latest lizard -m -T nloc=200 -T cyclomatic_complexity=15 -o report.txt`
